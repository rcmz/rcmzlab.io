---
date: 2025-03-09
title: ShaderV
---

The 21st century is a difficult time for computer graphic research. OpenGL is still well supported but it does not have access to all the modern programming features that researcher might want to toy with (ray tracing, mesh shaders, ...). Vulkan is the way forward but it is incredibly verbose, we are talking 500+ lines for a barebone "HelloTriangle" ! Don't get me wrong, Vulkan verbosity forces you to actually understand what you are doing (something that was not required with OpenGL), but it's a real pain when you want to hack something quickly. Such was the conundrum we had in our research team : lots of researchers wanted access to the latest GPU features, but almost no one wanted to go through the hardship of learning Vulkan. And even the brave one that did agreed that was not a great research environment. 

The remedy is quite obvious : find or develop a Vulkan wrapper. Of course I did not found an existing wrapper that functioned exactly how I wanted it to, so I developed mine : [ShaderV](https://gitlab.com/rcmz/shaderV3). The name is mashup of [shadertoy](https://www.shadertoy.com/) and Vulkan, because it is meant to be as easy to use as shadertoy, while giving you access to ~~all~~ most Vulkan feature. It does not claim to be efficient - there is plenty of deviceSyncronize - but it is dead easy to use, and still allows for all shader features.
