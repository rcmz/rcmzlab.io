---
date: 2025-03-08
title: FPV drone building and flying
---

FPV stands for "First Person View", and it refers to the practice of piloting an remote controlled aircraft through a form of VR headset. This allows the pilot to have a truly immersive flying experience, and is why I've fallen in love with it. (it also makes it much easier - imagine trying to drive you car while being outside of it !). FPV started in the 2010's with remote airplanes, but it really took of in the 2020's with quadcopters (most often simply referred as "drones"). Modern racing and freestyle quadcopters have incredible agility, they can easily go above 100km/h, and more importantly they can accelerate from 0 to 100 in less than a second. All this power right at your fingertip is really hard to control at first, but this difficulty is also what keeps it interesting in the long run.

FPV drones range all the way from palm sized sub 30g "tinyhoops" that are perfect for flying indoors, to giant 5Kg+ "cinelifters" that are used to fly big movie quality cameras. Regardless off their size, FPV drones are usually hand assembled by their pilot. There is a true ecosystem of drone parts from a myriad of vendors that are mostly intercompatible, in a way very reminiscent of PC building. This allows pilots to customize and upgrade their craft to their liking, and also makes FPV drones super reparable, a must when you like going really fast through really small holes ^^.

Sadly starting from 2023 FPV drones started being massively used for another purpose : warfare. Ukrainians discovered that slapping an explosive charge to an FPV drone makes it an incredibly cheap and effective weapon. It's tragic to see this hobby that was developed by enthusiasts "just to have fun" being repurposed for war. Let's hope that - just like aviation - FPV drones will be remembered in the long run as a a beautiful hobby and not an angel of death.
