---
date: 2024-07-07
title: How to make bad conference recordings listenable
---

Have you ever encountered a recording of a great conference, but the audio quality makes it very hard to listen to ?
This does not have to be a fatality, and can easily be fixed with an audio effect tool !
Many such tools can be used for this, but the one I will be presenting here is [Easy Effects](https://flathub.org/apps/com.github.wwmm.easyeffects).

If the recording has an annoying hissing noise in the background, you can use the **Noise Reduction** effect to remove it.
From my experience this one just works, no need to fuss around with the settings.
It has a tendency to mess up music though, so don't forget to disable it if the content in question is not strictly speech.

If the recording has bad stereo, like uncentered audio, or speech on one side and noise on the other, you can use the **Stereo Tools** effect.
This one is a bit more scary, with tons of settings that we don't need, but all the interesting stuff is in the **Stereo Matrix** tab.
Here, the first drop-down allows you to duplicate the right or left channel, or make an average of the two.
